Several free software created by [Adullact](https://adullact.org) and available under the [GNU AGPL v3](https://gitlab.adullact.net/adullact/pki/tajine/-/blob/main/LICENSE) license.

[ToC]

--------------------------------------

## Tajine

[![Project badge](https://gitlab.adullact.net/adullact/pki/tajine/-/badges/release.svg)](https://gitlab.adullact.net/adullact/pki/tajine/-/releases)
[![Project badge](https://gitlab.adullact.net/adullact/pki/tajine/badges/main/pipeline.svg)](https://gitlab.adullact.net/adullact/pki/tajine/pipelines?ref=main) 

Tajine, a **registration authority** (RA) frontend plugged to backend [**CFSSL**](https://github.com/cloudflare/cfssl).

- [`Tajine`, **source code**](https://gitlab.adullact.net/adullact/pki/tajine/)

--------------------------------------

## Puppet modules

### CFSSL

[![Project badge](https://gitlab.adullact.net/adullact/puppet-cfssl/-/badges/release.svg)](https://gitlab.adullact.net/adullact/puppet-cfssl/-/releases) 
[![Project badge](https://gitlab.adullact.net/adullact/puppet-cfssl/badges/main/pipeline.svg)](https://gitlab.adullact.net/adullact/puppet-cfssl/pipelines?ref=main) 
 [![Puppet CFSSL downloads](https://img.shields.io/puppetforge/dt/adullact/cfssl?label=Puppet%20CFSSL%20downloads)](https://forge.puppet.com/modules/adullact/cfssl/readme) 

 - [**CFSSL** Puppet module, **source code**](https://gitlab.adullact.net/adullact/puppet-cfssl/)
 - [**CFSSL** Puppet module on **Puppet Forge**](https://forge.puppet.com/modules/adullact/cfssl/readme)


### Tajine

[![Project badge](https://gitlab.adullact.net/adullact/puppet-tajine/-/badges/release.svg)](https://gitlab.adullact.net/adullact/puppet-tajine/-/releases) 
[![Project badge](https://gitlab.adullact.net/adullact/puppet-tajine/badges/main/pipeline.svg)](https://gitlab.adullact.net/adullact/puppet-tajine/pipelines?ref=main) 
[![Puppet Tajine downloads](https://img.shields.io/puppetforge/dt/adullact/tajine?label=Puppet%20Tajine%20downloads)](https://forge.puppet.com/modules/adullact/tajine/readme)

 - [**Tajine** Puppet module, **source code**](https://gitlab.adullact.net/adullact/puppet-tajine)
 - [**Tajine** Puppet module on **Puppet Forge**](https://forge.puppet.com/modules/adullact/tajine/readme)


--------------------------------------

## Vagrant VM 

> ⚠️ Warning : only for test or demonstration purposes

### Vagrant - Puppet CFSSL

[![Project badge](https://gitlab.adullact.net/adullact/pki/vagrant-puppet-cfssl/-/badges/release.svg)](https://gitlab.adullact.net/adullact/pki/vagrant-puppet-cfssl/-/releases)
[![Project badge](https://gitlab.adullact.net/adullact/pki/vagrant-puppet-cfssl/badges/main/pipeline.svg)](https://gitlab.adullact.net/adullact/pki/vagrant-puppet-cfssl/pipelines?ref=main) 

Build from scratch a running **CFSSL** server in a VM on your laptop.

- [`vagrant-puppet-cfssl`, **source code**](https://gitlab.adullact.net/adullact/pki/vagrant-puppet-cfssl/)

### Vagrant - Puppet TAJINE + Puppet CFSLL

[![Project badge](https://gitlab.adullact.net/adullact/pki/vagrant-tajine/-/badges/release.svg)](https://gitlab.adullact.net/adullact/pki/vagrant-tajine/-/releases)
[![Project badge](https://gitlab.adullact.net/adullact/pki/vagrant-tajine/badges/main/pipeline.svg)](https://gitlab.adullact.net/adullact/pki/vagrant-tajine/pipelines?ref=main) 

Build from scratch a running **Tajine** webapp and a running **CFSSL** server in a VM on your laptop.

- [`vagrant-tajine`, **source code**](https://gitlab.adullact.net/adullact/pki/vagrant-tajine/)

